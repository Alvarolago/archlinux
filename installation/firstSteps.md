#OBJETIVO DE ESTE DOCUMENTO
El propósito de este documento es la instalación de archlinux junto a windows de manera que al arrancar el pc se pueda seleccionar si iniciar con windows o con arch.

#ANTES DE NADA
Muy importante, además de seguir estos pasos tener siempre en cuanta la documentación oficial de archlinux: https://wiki.archlinux.org/title/Installation_guide

#PRIMEROS PASOS - PRE-INSTALACIÓN

Se necesitará un pendrive USB.
En primer lugar debemos descargar la ISO de archlinux desde la página oficial: https://archlinux.org/download/

Es recomendable hacer una verificación de la ISO para evitar problemas de que la ISO contenga software malicioso, pero en este caso omitiremos este paso, está perfectamente documentado en la guia de instalación de la web oficial de archlinux que hemos comentado al principio.

Para quemar la iso en el pendrive USB debemos usar un programa que se encargue de este proceso, en este caso usaremos un ordenador alternativo con windows 10 y usaremos el programa rufus.


