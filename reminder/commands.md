## ENLACES IMPORTANTES:
antonio sarosi-> https://github.com/antoniosarosi/dotfiles
:mundojuancri-> https://www.youtube.com/watch?v=LGTj32fBuMs


## PROGRAMAS INSTALADOS
- Gestor de ventanas:
| Nombre 	| Descripción 		| Ruta configuración 	|
| ------------- | --------------------- | ---------------------	|
| **Qtile**	| Gestor de ventanas    | ./config/qtile	|
	

## CONFIGURACIONES:
Ajustar brillo: $brightnessctl set +10%



## KALI:
| **CTRL + SHIFT + C**    | Copiar contenido seleccionado   |
| **CTRL + SHIFT + V**    | Pegar el contenido seleccionado |


## VIM:
| **En modo NORMAL pulsar u** | deshacer los cambios |
| **CTRL + R**                | rehacer los cambios  |

## ATAJOS DE TECLA VENTANAS
| Atajo                   | Acción                                       |
| ----------------------- | -------------------------------------------- |
| **mod + j**             | siguiente ventana                            |
| **mod + k**             | ventana previa                               |
| **mod + shift + h**     | aumentar master                              |
| **mod + shift + l**     | decrementar master                           |
| **mod + shift + j**     | mover ventana abajo                          |
| **mod + shift + k**     | mover ventana arriba                         |
| **mod + shift + f**     | pasar ventana a flotante                     |
| **mod + tab**           | cambiar la disposición de las ventanas       |
| **mod + [1-9]**         | cambiar al espacio de trabajo N (1-9)        |
| **mod + shift + [1-9]** | mandar ventana al espacio de trabajo N (1-9) |
| **mod + punto**         | enfocar siguiente monitor                    |
| **mod + coma**          | enfocar monitor previo                       |
| **mod + w**             | cerrar ventana                               |
| **mod + ctrl + r**      | reiniciar gestor de ventana                  |
| **mod + ctrl + q**      | cerrar sesión                                |


## ATAJOS DE TECLA APPS
| Atajo               | Acción                                 |
| ------------------- | -------------------------------------- |
| **mod + m**         | lanzar rofi                            |
| **mod + shift + m** | navegación (rofi)                      |
| **mod + b**         | lanzar navegador (firefox)             |
| **mod + e**         | lanzar explorador de archivos (thunar) |
| **mod + return**    | lanzar terminal (alacritty)            |
| **mod + r**         | redshift                               |
| **mod + shift + r** | parar redshift                         |
| **mod + s**         | captura de pantalla (scrot)            |


## MANTENIMIENTO DEL SISTEMA
- Liberación de espacio

1º: $du -sh /var/cache/pacman/pkg/ -> Ver espacio de cache utilizado
por pacman
2º: sudo pacman -Scc -> Limpiar el cache





